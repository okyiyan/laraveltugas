<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){
        //dd($request->all());
        //return view('posts.create');
        $query = DB::table('posts')->insert([
            "title"=>$request["title"],
            "body"=>$request["body"]
        ]);
        return redirect('/posts/create');
    }
}
