<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function index(){
        // $post = DB::table('cast')->get();
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        // $request->validate([
        //     'nama' => 'required',
        //     'umur' => 'required',
        //     'bio' => 'required'
        // ]);
        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request["nama"];
        // $cast->umur = $request["umur"];
        // $cast->bio = $request["bio"];
        // $cast->save();

        $cast = Cast:: create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast');
    }
    
    public function show($id)
    {
        //$cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        /*$request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);*/

        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request["nama"],
        //         'umur' => $request["umur"],
        //         'bio' => $request["bio"]
        //     ]);

        $update = Cast::where('id',$id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }
    
    public function destroy($id)
    {
        //$query = DB::table('cast')->where('id', $id)->delete();
        Cast::destroy($id);
        
        return redirect('/cast');
    }
}
