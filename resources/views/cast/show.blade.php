@extends('master')
@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-primary"> 
        <div class="card-header">
        <h2>Show Cast {{$cast->id}}</h2>
        </div>
        <div class="card-body">
        <h4>{{$cast->nama}}</h4>
        <h4>{{$cast->umur}}</h4>
        <p>{{$cast->bio}}</p>
        </div>
    </div>
</div>
@endsection