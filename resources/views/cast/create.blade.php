@extends('master')

@section('content')
<div class="ml-3 mt-3 mr-3">
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="forms" action="/cast" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Name</label>
          <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter Name">
        </div>
        <div class="form-group">
          <label for="umur">Age</label>
          <input type="integer" class="form-control" id="umur" name="umur" placeholder="Enter Age">
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea type="longtext" class="form-control" rows="3" id="bio" name="bio" placeholder="Enter ..."></textarea>
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
</div>
@endsection