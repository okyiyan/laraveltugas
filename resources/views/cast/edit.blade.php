@extends('master')

@section('content')
<div class="ml-3 mt-3 mr-3">
<div class="card card-primary">
    <div class="card-header">
    <h2>Edit Cast {{$cast->id}}</h2>
    </div>
    <div class="m-3">
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Name</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Age</label>
            <input type="integer" class="form-control" id="umur" name="umur" value="{{$cast->umur}}" placeholder="Enter Age">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea type="longtext" class="form-control" rows="3" value="{{$cast->bio}}" id="bio" name="bio" placeholder="Masukkan Bio"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror        
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </div>
</div>
    </form>
</div>

@endsection