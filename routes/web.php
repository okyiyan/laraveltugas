<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function () {
//     return view('master');
// });

// Route::get('/table', function () {
//     return view('items.table');
// });

// Route::get('/data-table', function () {
//     return view('items.datatable');
// });

// Route::get('/posts/create','PostController@create');

// Route::post('/posts','PostController@store');


// Route::get('/cast/create','CastController@create');

// Route::post('/cast','CastController@store');

// Route::get('/cast','CastController@index');

// Route::get('/cast/{cast_id}','CastController@show');

// Route::get('/cast/{cast_id}/edit','CastController@edit');

// Route::put('/cast/{cast_id}','CastController@update');

// Route::delete('/cast/{cast_id}','CastController@destroy');

Route::resource('casting', 'CastingController');

Route::resource('cast','CastController');